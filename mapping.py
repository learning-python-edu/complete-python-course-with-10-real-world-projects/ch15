import folium
import pandas as pd

data = pd.read_csv('Volcanoes.csv')
lat = data['LAT'].tolist()
lon = data['LON'].tolist()
elev = data['ELEV'].tolist()


def color_producer(elevation):
    if elevation < 1000:
        return 'blue'
    elif elevation < 3000:
        return 'orange'
    else:
        return 'red'


mapi = folium.Map(location=[38.58, -99.09], zoom_start=6, tiles='Stadia.StamenTerrain')

fgv = folium.FeatureGroup(name="Volcanoes")
for lt, ln, el in zip(lat, lon, elev):
    popup = folium.Popup(f"{el} m", min_width=20, max_width=400)
    fgv.add_child(folium.CircleMarker(location=[lt, ln], radius=6, popup=popup,
                                      fill_color=color_producer(el), color='grey', fill_opacity=0.7))

fgp = folium.FeatureGroup(name="Population")
fgp.add_child(folium.GeoJson(data=(open('world.json', 'r', encoding='utf-8-sig').read()),
                             style_function=lambda x: {'fillColor': 'yellow' if x['properties']['POP2005'] < 10000000
                             else 'orange' if x['properties']['POP2005'] < 20000000 else 'red'}))

mapi.add_child(fgv)
mapi.add_child(fgp)
mapi.add_child(folium.LayerControl())

mapi.save('output/map1.html')
